#!/usr/bin/bash

rosservice call turtle1/set_pen 0 0 0 1 1
rosservice call turtle1/teleport_absolute 2 4 0
rosservice call turtle1/set_pen 150 0 0 2 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.5, 0, 0]' '[0,0,0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3, 0, 0]' '[0,0,3]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5, 0, 0]' '[0,0,3]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.5, 0, 0]' '[0,0,1]'

rosservice call spawn 8 5.5 90 "turtle2"
rosservice call /turtle2/set_pen 0 200 0 2 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[4.2, 0, 0]' '[0,0,3]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[4.2, 0, 0]' '[0,0,3]'
rosservice call /turtle2/teleport_relative 0 180
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[2, 0, 0]' '[0,0,0]'
